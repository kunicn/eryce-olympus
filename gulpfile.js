'use strict';

// GULP MODULES
var gulp = require('gulp'); // Gulp
var sass = require('gulp-sass'); // Gulp sass
var browserSync = require('browser-sync').create(); // Browser sync
var useref = require('gulp-useref'); // Gulp useref
var uglify = require('gulp-uglify'); // Gulp uglify
var gulpIf = require('gulp-if'); // Gulp if
var cssnano = require('gulp-cssnano'); // Gulp cssnano
var imagemin = require('gulp-imagemin'); // Gulp imagemin 
var cache = require('gulp-cache'); // Gulp cache
var del = require('del'); // Gulp del
var runSequence = require('run-sequence'); // Gulp run sequence

// GULP TASKS

// I-01-02-Gulp sass 2
gulp.task('sass', function() {
  return gulp.src('app/scss/**/*.scss') 
    .pipe(sass())
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

// I-03-Browser sync
gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: 'app'
    },
  })
})

// I-02-04-Gulp watch 4
gulp.task('watch', ['browserSync', 'sass'], function (){
  gulp.watch('app/scss/**/*.scss', ['sass']);
  gulp.watch('app/*.html', browserSync.reload); 
  gulp.watch('app/js/**/*.js', browserSync.reload); 
});



// II-01-03-Gulp useref 3
gulp.task('useref', function(){
  return gulp.src('app/*.html')
    .pipe(useref())
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulpIf('*.css', cssnano()))
    .pipe(gulp.dest('dist'))
});

// II-02-03a-Gulp imagemin 3a
gulp.task('minimages', function(){
  return gulp.src('app/img/**/*.+(png|jpg|jpeg|gif|svg)')
  .pipe(cache(imagemin({
      interlaced: true
    })))
  .pipe(gulp.dest('dist/img'))
});

// II-02-04b-Gulp imagemin 3b
gulp.task('minseoimages', function(){
  return gulp.src(['app/**/*.+(png|jpg|jpeg|gif|svg|ico)', '!app/vendor/**'])
  .pipe(cache(imagemin({
      interlaced: true
    })))
  .pipe(gulp.dest('dist'))
});

// Copy Font awesome fonts
gulp.task('fonts', function() {
  return gulp.src('app/vendor/font-awesome/fonts/**/*')
  .pipe(gulp.dest('dist/fonts'))
})

// II-04-Gulp del 
gulp.task('clean:dist', function() {
  return del.sync('dist');
})

// II-05-Cleans local cache
gulp.task('cache:clear', function (callback) {
return cache.clearAll(callback)
})

// I group - Development
gulp.task('default', function (callback) {
  runSequence(['sass','browserSync', 'watch'],
    callback
  )
})

// II group - Development => Production
gulp.task('build', function (callback) {
  runSequence('clean:dist', 
    ['sass', 'useref', 'minimages', 'minseoimages', 'fonts'],
    callback
  )
})
